# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "keinmark"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "keinmark"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', doublequotes or '@'."
  default     = "keinmark"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYpL97mfT0MatY7C6SFiw+WJiUOkNciCasRf4CVbAeRRYQI6t1+6aWhEhJYkLdWSewguL7Z/p4xLwPDsPVEN0sW0J3qRDzqTpPD9zCogvKw7HDKSM3pMVJZaeBgeH/EI0OTKb/fE2ScJb7T/PIjan2fhmJn/UBD7ahNKfRt4ZphhG0sIoMDE450cr9/2raVKatXFrt/SDZ/8Rhami43vMwWXovynX05c6k/h+YeIqG0Qjs4zlCzV+OsfJiotkN9C0QMovpeQ5JRxtYOx7Wvl4mQY5FQxmFfux6MrakiXO1k7RUJCB37qmjIgWLiWtQd65iSNqOn2qthfqDKlxGSTfn mark@mark-VirtualBox"
}
